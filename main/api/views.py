from rest_framework import viewsets
from stats.models import Authors
from stats.helpers import countWords
from .serializers import AuthorsSerializer, WordsSerializer
from rest_framework.response import Response


class AuthorsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Authors.objects.all()
    serializer_class = AuthorsSerializer

    # Override list method to craft custom response json for GET /authors
    def list(self, request, *args, **kwargs):
        # Original code
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        # Custom code
        response = {}

        for item in serializer.data:
            response[item['url_name']] = item['name']

        return Response(response)

class AuthorViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Authors.objects.all()
    serializer_class = WordsSerializer
    lookup_field = 'url_name'

    # Override retrieve method to craft custom response json for /stats/<author's name>
    def retrieve(self, request, *args, **kwargs):
        # Original code
        instance = self.get_object()
        serializer = self.get_serializer(instance)

        # Custom code
        return Response(countWords(serializer.data['all_words'].split(' ')))

    # Override list method to craft custom response json for /stats
    def list(self, request, *args, **kwargs):
        # Original code
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        # Custom code
        words = ''

        for item in serializer.data:
            words += item['all_words']

        return Response(countWords(words.split(' ')))
