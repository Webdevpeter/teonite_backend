# Teonite Crawler Backend

## Build instructions
1. `git clone https://gitlab.com/Webdevpeter/teonite_backend.git`
2. `cd teonite_backend`
3. `docker-compose up`
4. Wait for crawler to finish to see results on endpoints.
